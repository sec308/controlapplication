package com.example.gencay.uygulama;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button ileri,geri,sag,sol;
    Switch mySwitch;
    EditText ed_ip,ed_port;
    MyClientTask myClientTask;
    int BAGLİDEGİL=0;
    int BAGLİ=1;
    int baglanti_durumu=BAGLİDEGİL;
    int IPvePORTGIRILMEDI=0;
    String DEFAULTTEXT=Integer.toString(IPvePORTGIRILMEDI);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sol=(Button)findViewById(R.id.button);
        sag=(Button)findViewById(R.id.button1);
        ileri=(Button)findViewById(R.id.button2);
        geri=(Button)findViewById(R.id.button3);
        mySwitch = (Switch) findViewById(R.id.switch1);
        ed_ip=(EditText)findViewById(R.id.editText2);
        ed_port=(EditText)findViewById(R.id.editText);


        ed_port.setText(DEFAULTTEXT);
        ed_ip.setText(DEFAULTTEXT);




        /////////////////////////////SWITCH KONTROL
        mySwitch.setChecked(false);
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {    //switchin açılması durumunda textlerden girilen port ve ıp ile servere bağlanılıyor.

                    if(ed_ip.getText().toString().equals(DEFAULTTEXT) && ed_port.getText().toString().equals(DEFAULTTEXT)){
                        Toast.makeText(getApplicationContext(), "IP veya PORT Hatalı.", Toast.LENGTH_LONG).show();
                        mySwitch.setChecked(false);
                      }
                    else{
                        myClientTask = new MyClientTask(ed_ip.getText().toString(),Integer.parseInt(ed_port.getText().toString()));
                        myClientTask.execute();
                        baglanti_durumu=BAGLİ;
                        Toast.makeText(getApplicationContext(), "Araca bağlanıldı.", Toast.LENGTH_LONG).show();

                    }
                }
                else {  // Görünürde bağlantıkesiliyor.
                    baglanti_durumu=BAGLİDEGİL;
                    Toast.makeText(getApplicationContext(), "Araç bağlantısı kesildi.", Toast.LENGTH_LONG).show();
                }
            }
        });
        //////////YON BUTTONLARI KONTROL
        sol.setOnTouchListener(new View.OnTouchListener(){                             //ileri butonu
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:{//butona basıldığı sürece çalışacak
                        if( baglanti_durumu==BAGLİ){//baglanti var ise drone yönlendiriliyor.
                              Toast.makeText(getApplicationContext(), "Sola Yatıyor.", Toast.LENGTH_SHORT).show();}
                        else{
                              Toast.makeText(getApplicationContext(), "Baglanti yok.", Toast.LENGTH_SHORT).show();}
                        break;
                    }
                    case MotionEvent.ACTION_UP:{//butondan parmak çekildi.
                        if(baglanti_durumu==BAGLİ){//baglanti var ise drone yönlendiriliyor.
                            Toast.makeText(getApplicationContext(), "Dengeleniyor.", Toast.LENGTH_SHORT).show();}
                        else{
                            Toast.makeText(getApplicationContext(), "Baglanti yok.", Toast.LENGTH_SHORT).show();}

                       break;
                    }
                }
                return true;
            }

        });

        sag.setOnTouchListener(new View.OnTouchListener(){                             //ileri butonu
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:{
                         Toast.makeText(getApplicationContext(), "Sağa Yatıyor.", Toast.LENGTH_SHORT).show();
                         break;
                    }
                    case MotionEvent.ACTION_UP:{
                         Toast.makeText(getApplicationContext(), "Dengeleniyor.", Toast.LENGTH_SHORT).show();
                         break;
                    }
                }
                return true;
            }
        });

        ileri.setOnTouchListener(new View.OnTouchListener(){                             //ileri butonu
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                   switch (event.getAction()) {
                       case MotionEvent.ACTION_DOWN: {
                           if(baglanti_durumu==BAGLİ) {
                           Toast.makeText(getApplicationContext(), "İleri gitmek için öne yatıyor.", Toast.LENGTH_SHORT).show();}
                           else{
                               Toast.makeText(getApplicationContext(), "Baglanti yok.", Toast.LENGTH_SHORT).show();
                           }
                           break;
                       }
                       case MotionEvent.ACTION_UP: {
                           if(baglanti_durumu==BAGLİ) {
                               Toast.makeText(getApplicationContext(), "Dengeleniyor.", Toast.LENGTH_SHORT).show();}
                           else{
                               Toast.makeText(getApplicationContext(), "Baglanti yok.", Toast.LENGTH_SHORT).show();
                           }

                           break;
                       }
                   }


              return true;
            }
        });

        geri.setOnTouchListener(new View.OnTouchListener(){                             //ileri butonu
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_DOWN:{
                         Toast.makeText(getApplicationContext(), "Geri gitmek için arkaya yatıyor.", Toast.LENGTH_SHORT).show();
                         break;
                    }
                    case MotionEvent.ACTION_UP:{
                         Toast.makeText(getApplicationContext(), "Dengeleniyor.", Toast.LENGTH_SHORT).show();
                         break;
                    }
                }
                return true;
            }
        });
        //////////////////////////////////
    }
}





















