package com.example.gencay.uygulama;

import android.os.AsyncTask;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class MyClientTask extends AsyncTask<Void, Void, Void> {

    private String dstAddress;
    private int dstPort;
    private Socket socket;
    DataOutputStream out;

    MyClientTask(String addr, int port){
        dstAddress = addr;
        dstPort = port;
    }


    @Override
    protected Void doInBackground(Void... arg0) {

        try {
            socket = new Socket(dstAddress, dstPort);
            OutputStream outToServer = socket.getOutputStream();
            out = new DataOutputStream(outToServer);

        } catch (UnknownHostException e) {
            System.err.println("Baglanamadi");
            e.printStackTrace();

        } catch (IOException e) {
            System.err.println("Baglanamadi");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {

        super.onPostExecute(result);
    }

}
